var webpackConfig = require('./webpack-configs/webpack.config.test');

module.exports = function(config) {
    config.set({
        basePath: '',

        frameworks: ['jasmine', 'source-map-support'],

        files: ['karma.entry.ts'],

        port: 9876,
        logLevel: config.LOG_INFO,
        colors: true,
        autoWatch: true,

        browsers: ['PhantomJS'],

        webpack: webpackConfig,

        webpackMiddleware: {
            stats: 'errors-only'
        },

        webpackServer: {
            noInfo: true
        },

        reporters: ['progress', 'coverage'],

        preprocessors: {
            'karma.entry.ts': ['webpack']
        },

        coverageReporter: {
            reporters: [
                {
                    type: 'json',
                    dir: 'coverage/',
                    subdir: '.',
                    file: 'coverage.json'
                }
            ]
        },

        singleRun: true
    });
};
