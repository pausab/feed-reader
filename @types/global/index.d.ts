interface GoogleApi {
    feeds?: GoogleFeedApi;
}

interface GoogleFeedApi {
    Feed?: any;
}

declare var google: GoogleApi;

interface Window {
    google: GoogleApi;
}
