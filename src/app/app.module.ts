import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FeedModule} from './feed-module';
import {CommonModule} from './common-module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        FeedModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
