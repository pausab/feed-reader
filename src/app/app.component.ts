import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoaderService } from './common-module/services';
import { FeedService } from './feed-module/services';
import { Feed, OrderByField } from './feed-module/entities';

@Component({
    selector: 'app',
    template: require('./app.component.html')
})
export class AppComponent implements OnInit {
    feedUrl: string;
    feed: Feed;
    feedHistory: string[] = [];
    orderByExpression: string[] = [];
    orderableFields: OrderByField[] = [
        {
            name: 'Published date',
            key: 'publishedDate'
        }, {
            name: 'Title',
            key: 'title'
        }
    ];

    constructor(private feedService: FeedService,
                private loaderService: LoaderService) {
        this.feedUrl = feedService.getLastFeedUrl();
    };

    ngOnInit(): void {
        if (this.feedUrl) {
            this.loadFeed();
        }
    }

    orderChanged(value: string): void {
        this.orderByExpression = [value];
    }

    onSubmit(form: NgForm): void {
        if (form.invalid) {
            return;
        }
        this.loadFeed();
    }

    loadFeed(): void {
        this.loaderService.setState(true);
        this.feedService.getFeed(this.feedUrl)
            .then((result: Feed) => {
                this.feed = result;
                this.loaderService.setState(false);
                this.feedHistory = this.feedService.getFeedHistory();
            });
    }

    changeFeed(feedUrl: string): void {
        this.feedUrl = feedUrl;
        this.loadFeed();
    }
}
