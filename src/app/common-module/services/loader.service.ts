import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class LoaderService {
    public state: Subject<boolean> = new Subject<boolean>();
    private show: boolean = false;

     setState(state: boolean): void {
       this.show = state;
       this.state.next(state);
     }

     getState(): boolean {
       return this.show;
     }
}
