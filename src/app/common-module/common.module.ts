import {NgModule} from '@angular/core';
import {LoaderComponent, NavComponent} from './components';
import {LoaderService} from './services';
import {BrowserModule} from '@angular/platform-browser';
import {MomentModule} from 'angular2-moment';
import {NgbModalModule, NgbCollapseModule, NgbDropdownModule, NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule } from '@angular/forms';

const declarations: any[] = [
    LoaderComponent,
    NavComponent
];

@NgModule({
    declarations: declarations,
    exports: [
        BrowserModule,
        FormsModule,
        MomentModule,
        NgbCollapseModule,
        NgbModalModule,
        NgbDropdownModule,
        ...declarations
    ],
    imports: [
        FormsModule,
        BrowserModule,
        MomentModule,
        NgbModalModule.forRoot(),
        NgbCollapseModule,
        NgbDropdownModule
    ],
    providers: [
        LoaderService,
        NgbDropdownConfig
    ]
})
export class CommonModule { }
