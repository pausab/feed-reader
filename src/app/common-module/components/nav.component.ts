import {Component, Input} from '@angular/core';

@Component({
    selector: 'ui-nav',
    template: require('./nav.component.html')
})
export class NavComponent {
    collapsed: boolean;

    toggle(): void {
        this.collapsed = !this.collapsed;
    }
}
