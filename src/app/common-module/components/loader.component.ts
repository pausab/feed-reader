import {LoaderService} from '../services';
import {Component, Input} from '@angular/core';

@Component({
    selector: 'ui-loader',
    template: require('./loader.component.html')
})
export class LoaderComponent {
  @Input()
  show: boolean;

  constructor(private loaderService: LoaderService) {
      this.loaderService.state.subscribe((show: boolean) => {
        this.show = show;
      });
  }
}
