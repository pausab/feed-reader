import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {AppModule} from './app.module';

if(!location.host.includes('localhost')) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
