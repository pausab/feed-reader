import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'navigate-modal',
    template: require('./navigate-modal.component.html')
})
export class NavigateModalComponent {
    title: string;

    constructor(public activeModal: NgbActiveModal) {}
}
