import { Component, Input } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NavigateModalComponent } from './navigate-modal.component';
import { FeedEntry } from '../entities';

@Component({
    selector: 'ui-feed-list',
    template: require('./feed-list.component.html')
})
export class FeedListComponent {
    @Input() feedEntries: FeedEntry[];
    @Input() orderBy: string[] = [];

    constructor(private modalService: NgbModal) {}

    navigate(feedEntry: FeedEntry): void {
        let modalref: NgbModalRef = this.modalService.open(NavigateModalComponent, { windowClass: 'animated slideInDown modal-custom-animations' });

        modalref.componentInstance.title = feedEntry.title;
        modalref.result
            .then(() => {
                window.open(feedEntry.link, '_blank');
            })
            .catch(() => null);
    }
}
