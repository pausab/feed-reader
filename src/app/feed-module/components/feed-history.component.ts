import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ui-feed-history',
    template: require('./feed-history.component.html')
})
export class FeedHistoryComponent {
    @Input() feedHistory: string[];
    @Output() onFeedHistoryNavigate: EventEmitter<string> = new EventEmitter<string>();

    navigateToHistoryItem(feedHistoryItem: string): void {
        this.onFeedHistoryNavigate.emit(feedHistoryItem);
    }
}
