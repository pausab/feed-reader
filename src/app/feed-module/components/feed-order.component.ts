import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DirectionFilter, OrderByField, Filter } from '../entities';

@Component({
    selector: 'ui-feed-order',
    template: require('./feed-order.component.html')
})
export class FeedOrderComponent implements OnChanges {
    @Input() fields: OrderByField[];
    @Output() orderByChanged: EventEmitter<string> = new EventEmitter<string>();
    directionFilters: DirectionFilter[] = [
        {
            name: 'Descending',
            key: ''
        },
        {
            name: 'Ascending',
            key: '-'
        }
    ];
    currentFilter: Filter = {
        field: {
            name: '',
            key: ''
        },
        direction: this.directionFilters[1]
    };

    ngOnChanges(): void {
        if(this.fields && Array.isArray(this.fields)) {
            this.currentFilter.field = this.fields[0];
        }
    }

    setField(field: OrderByField): void {
        this.currentFilter.field = field;
        this.emmitFilterData();
    }

    setDirection(direction: DirectionFilter): void {
        this.currentFilter.direction = direction;
        this.emmitFilterData();
    }

    private emmitFilterData(): void {
        this.orderByChanged.emit(`${this.currentFilter.direction.key}${this.currentFilter.field.key}`);
    }
}
