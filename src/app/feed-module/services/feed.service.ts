import { Injectable } from '@angular/core';
import { Feed, FeedEntry } from '../entities';

const MAX_ENTRIES: number = 10;

@Injectable()
export class FeedService {
    private feedHistory: string[] = [];

    getFeed(feedUrl: string): Promise<Feed> {
        this.addToHistory(feedUrl);

        return new Promise<Feed>((resolve) => {
            let feed: any = new google.feeds.Feed(feedUrl);

            feed.setNumEntries(MAX_ENTRIES);
            feed.load((result: any) => {
                localStorage.setItem('lastVisitedFeed', feedUrl);
                resolve(this.formatResult(result));
            });
        });
    }

    getFeedHistory(): string[] {
        return this.feedHistory;
    }

    getLastFeedUrl(): string {
        return localStorage.getItem('lastVisitedFeed');
    }

    private addToHistory(feedUrl: string): void {
        let feedIndex: number = this.feedHistory.indexOf(feedUrl);

        if(feedIndex === -1) {
            this.feedHistory.push(feedUrl);
        }
    }

    private formatResult(feed: Feed): Feed {
        feed.feed.entries = feed.feed.entries.map((entry: FeedEntry) => {
            return {...entry, publishedDate: this.fixDate(entry.publishedDate)};
        });
        return feed;
    };

    private fixDate(date: string): string {
        if (!date) {
            return new Date().toISOString();
        }
        return new Date(date).toISOString();
    }
}
