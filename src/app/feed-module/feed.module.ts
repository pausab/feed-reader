import {NgModule} from '@angular/core';
import {NavigateModalComponent} from './components/navigate-modal.component';
import {FeedOrderComponent} from './components/feed-order.component';
import {FeedListComponent} from './components/feed-list.component';
import {FeedHistoryComponent} from './components/feed-history.component';
import {OrderByPipe} from './pipes/order-by.pipe';
import {FeedService} from './services';
import {CommonModule} from '../common-module';

const declarations: any[] = [
    NavigateModalComponent,
    OrderByPipe,
    FeedOrderComponent,
    FeedListComponent,
    FeedHistoryComponent
];

@NgModule({
    declarations: declarations,
    exports: declarations,
    entryComponents: [NavigateModalComponent],
    imports: [
        CommonModule
    ],
    providers: [
        FeedService
    ]
})
export class FeedModule { }
