export interface Feed {
    feed?: FeedInfo;
}

export interface FeedInfo {
    entries: FeedEntry[];
}

export interface FeedEntry {
    title: string;
    link: string;
    publishedDate: string;
    contentSnippet: string;
    categories: string[];
}
