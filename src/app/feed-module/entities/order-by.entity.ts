export interface DirectionFilter {
    key: string;
    name: string;
}

export interface OrderByField {
    key: string;
    name: string;
}

export interface Filter {
    field: OrderByField;
    direction: DirectionFilter;
}
