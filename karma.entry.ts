window.google = {};
import 'core-js/es6';
import 'reflect-metadata';
import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
import 'rxjs/Rx';

import 'ts-helpers';

import { TestBed } from '@angular/core/testing';
import * as browser from '@angular/platform-browser-dynamic/testing';

TestBed.initTestEnvironment(
    browser.BrowserDynamicTestingModule,
    browser.platformBrowserDynamicTesting()
);

let testContext: any = (<{ context?: Function }>require).context('./tests', true, /\.spec\.ts/);
testContext.keys().forEach(testContext);

let coverageContext: any = (<{ context?: Function }>require).context('./src/app', true, /^(?!\.(\/|\\)main\.ts$).*\.ts$/);
coverageContext.keys().forEach(coverageContext);
