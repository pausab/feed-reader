var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ExtractSASS = new ExtractTextPlugin(`css/[name].[contenthash].css`);

module.exports = {
    entry: {
        vendor: path.join(__dirname, '..', 'src', 'vendor.ts'),
        polyfills: path.join(__dirname, '..', 'src', 'polyfills.ts'),
        app: path.join(__dirname, '..', 'src/app', 'main.ts')
    },

    output: {
        path: './bin',
        filename: 'js/[name].[chunkhash].js'
    },

    resolve: {
        extensions: ['', '.js', '.ts']
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts'
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.(woff|woff2|ttf|eot|svg)$/,
                loader: 'file?name=fonts/[name].[ext]'
            },
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                loader: 'file?name=[path]/[name].[ext]'
            },
            {
                test: /\.scss$/,
                loader: ExtractSASS.extract(['css', 'sass'])
            }
        ]
    },

    progress: true,
    colors: true,

    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),

        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        ExtractSASS
    ]
};
