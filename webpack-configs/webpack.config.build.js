var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var mainConfig = require('./webpack.config.js');

module.exports = webpackMerge(mainConfig, {
    htmlLoader: {
        minimize: false
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
          output: { comments: false },
          mangle: { screw_ie8: true, keep_fnames: true }
      })
    ],

    ts: {
        compilerOptions: {
            sourceMap: false,
            watch: false
        }
    }
});
