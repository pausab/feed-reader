module.exports = {
    devtool: 'inline-source-map',

    resolve: {
        extensions: ['', '.ts', '.js'],
        modulesDirectories: ['node_modules', 'src']
    },

    ts: {
        compilerOptions: {
            sourceMap: false,
            inlineSourceMap: true
        }
    },

    verbose: true,

    module: {
        loaders: [{
            test: /\.ts$/,
            loader: 'ts'
        }, {
            test: /\.html$/,
            loader: 'html'
        }, {
            test: /\.scss$/,
            loader: 'css-loader!sass-loader'
        }],
        postLoaders: [{ // << add subject as webpack's postloader
            test: /\.ts$/,
            exclude: ['tests', /\.(e2e|spec)\.ts$/],
            loader: 'istanbul-instrumenter'
        }]
    }
};
