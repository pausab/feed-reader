var mainConfig = require('./webpack.config');
var webpackMerge = require('webpack-merge');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = webpackMerge(mainConfig, {
    devtool: 'source-map',

    devServer: {
        contentBase: 'bin/',
        port: 9998,
        historyApiFallback: true
    }
});
