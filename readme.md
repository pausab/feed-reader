## Running the project:

Run these commands inside the project folder where `package.json` is located.

`npm i`

`npm start`

app runs on http://localhost:9998

## Running the tests:

Assuming that `npm i` has been run, just run the command below.

`npm test`

## Coverage report:

To view the coverage report you need to run two commands provided below

`npm test && npm run coverage`

The tests will pass and a coverage report will be generated inside the
project folder `{projectDirectory}/coverage/lcov-report/index.html`

View the file `index.html` with any web browser.

## Other scripts:

`npm serve:dev` - serves not minified code withing webpack dev server, watches for code changes
(longer version of `npm start`)

`npm run build` - build the project for production ready deployment inside the `bin` folder.

## Additional notes:

Did not do testing on `order-by.pipe.ts` since it's a third party code, added a comment.