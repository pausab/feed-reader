import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { NavComponent } from '../../../src/app/common-module/components/nav.component';
import { CommonModule } from '../../../src/app/common-module';

let navComponent: NavComponent;

describe('NavComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule
            ]
        });
    });

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<NavComponent> = TestBed.createComponent(NavComponent);

            navComponent = fixture.componentInstance;
        });
    }));

    it('should toggle collapsed state', () => {
        let collapsedState: boolean = navComponent.collapsed;

        navComponent.toggle();

        expect(navComponent.collapsed).not.toEqual(collapsedState);
    });
});
