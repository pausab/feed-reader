import { async, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { LoaderComponent } from '../../../src/app/common-module/components/loader.component';
import { LoaderService } from '../../../src/app/common-module/services';
import { CommonModule } from '../../../src/app/common-module';

let loaderComponent: LoaderComponent,
    loaderService: LoaderService;

describe('LoaderComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule
            ]
        });
    });

    beforeEach(inject([LoaderService],(_loaderService_: LoaderService) => {
                loaderService = _loaderService_;
            }));

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<LoaderComponent> = TestBed.createComponent(LoaderComponent);

            loaderComponent = fixture.componentInstance;
        });
    }));

    it('should react to loader service state changes', () => {
        loaderService.setState(true);

        expect(loaderComponent.show).toEqual(true);
    });
});
