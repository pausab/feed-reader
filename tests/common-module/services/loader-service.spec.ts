import { TestBed, inject } from '@angular/core/testing';
import { LoaderService } from '../../../src/app/common-module/services';
import { CommonModule } from '../../../src/app/common-module';

let loaderService: LoaderService;

describe('LoaderService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule
            ]
        });
    });

    beforeEach(inject([LoaderService],(_loaderService_: LoaderService) => {
                loaderService = _loaderService_;
    }));


    it('should set state to true', () => {
        loaderService.setState(true);

        expect(loaderService.getState()).toEqual(true);
    });

    it('should set state to false', () => {
        loaderService.setState(false);

        expect(loaderService.getState()).toEqual(false);
    });
});
