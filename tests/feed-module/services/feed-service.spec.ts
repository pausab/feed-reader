import { TestBed, inject } from '@angular/core/testing';
import { FeedService } from '../../../src/app/feed-module/services';
import { FeedModule } from '../../../src/app/feed-module';

let feedService: FeedService;

let feedResultMock: any;

class FeedMock {
    load(callback: Function): void {
        callback(feedResultMock);
    }
    setNumEntries(): void{}
}

describe('LoaderService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FeedModule
            ]
        });
    });

    beforeEach(inject([FeedService],(_feedService_: FeedService) => {
                feedService = _feedService_;
                google = {
                    feeds: {
                        Feed: FeedMock
                    }
                };
                feedResultMock = {
                    feed: {
                        entries: [
                            {
                                title: 'TestEntry',
                                publishedDate: new Date().toString()
                            },
                            {
                                title: 'TestEntry',
                                publishedDate: ''
                            }
                        ]
                    }
                };
    }));


    it('should format date to ISO string after aquiring entries', () => {
        let expectedDate: string = new Date(feedResultMock.feed.entries[0].publishedDate).toISOString();

        feedService.getFeed('TestUrl').then((result: any) => {
            expect(result.feed.entries[0].publishedDate).toEqual(expectedDate);
        });
    });

    it('should set a new date if date is not provided', () => {
        let expectedDate: string = feedResultMock.feed.entries[1].publishedDate;

        feedService.getFeed('TestUrl').then((result: any) => {
            expect(result.feed.entries[1].publishedDate).not.toEqual(expectedDate);
        });
    });

    it('should add a history item when getting a feed', () => {
        feedService.getFeed('TestUrl');

        expect(feedService.getFeedHistory().length).toEqual(1);
    });

    it('should add last url to local storage when getting a feed', () => {
        let testUrl: string = 'SomTestingUrl.com';
        feedService.getFeed(testUrl);

        expect(feedService.getLastFeedUrl()).toEqual(testUrl);
    });
});
