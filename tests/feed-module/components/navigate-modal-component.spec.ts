import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { NavigateModalComponent } from '../../../src/app/feed-module/components/navigate-modal.component';
import { FeedModule } from '../../../src/app/feed-module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

let navigateModalComponent: NavigateModalComponent;

describe('NavigateModalComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FeedModule
            ],
            providers: [
                NgbActiveModal
            ]
        });
    });

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<NavigateModalComponent> = TestBed.createComponent(NavigateModalComponent);

            navigateModalComponent = fixture.componentInstance;
            navigateModalComponent.title = 'TestPassed';
        });
    }));

    it('should init successfuly and contain a title specified', () => {
        expect(navigateModalComponent.title).toEqual('TestPassed');
    });
});
