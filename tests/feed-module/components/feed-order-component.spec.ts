import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { FeedOrderComponent } from '../../../src/app/feed-module/components/feed-order.component';
import { FeedModule } from '../../../src/app/feed-module';
import { OrderByField, DirectionFilter } from '../../../src/app/feed-module/entities';

let feedOrderComponent: FeedOrderComponent,
    testFixture: ComponentFixture<FeedOrderComponent>;

describe('FeedOrderComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FeedModule
            ]
        });
    });

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            testFixture = TestBed.createComponent(FeedOrderComponent);

            feedOrderComponent = testFixture.componentInstance;
            feedOrderComponent.fields = [
                {
                    name: 'TestField1',
                    key: 'TestField1'
                },
                {
                    name: 'TestField2',
                    key: 'TestField2'
                }
            ];
        });
    }));

    it('should change current filter value if predefinedFields changes', () => {
        let newField: OrderByField = {
            name: 'TestField3',
            key: 'TestField3'
        };

        feedOrderComponent.fields.unshift(newField);

        feedOrderComponent.ngOnChanges();

        expect(feedOrderComponent.currentFilter.field).toEqual(newField);
    });

    it('should not change current filter value if predefinedFields is not an array or null', () => {
        let currentFilter: OrderByField = feedOrderComponent.currentFilter.field;
        feedOrderComponent.fields = null;

        expect(feedOrderComponent.currentFilter.field).toEqual(currentFilter);

        feedOrderComponent.ngOnChanges();

        expect(feedOrderComponent.currentFilter.field).toEqual(currentFilter);
    });

    it('should set new filed as current filter', () => {
        let newField: OrderByField = {
            name: 'TestField3',
            key: 'TestField3'
        };

        feedOrderComponent.setField(newField);

        expect(feedOrderComponent.currentFilter.field).toEqual(newField);
    });

    it('should set new direction as current filter', () => {
        let newDirectionFilter: DirectionFilter = feedOrderComponent.directionFilters[0];

        expect(feedOrderComponent.currentFilter.direction).not.toEqual(newDirectionFilter);

        feedOrderComponent.setDirection(newDirectionFilter);

        expect(feedOrderComponent.currentFilter.direction).toEqual(newDirectionFilter);
    });

    it('should emit an event that filter changed on direction or field change', () => {
        spyOn(feedOrderComponent.orderByChanged, 'emit');
        let newDirectionFilter: DirectionFilter = feedOrderComponent.directionFilters[0];
        let newFiled: DirectionFilter = feedOrderComponent.fields[1];

        feedOrderComponent.setDirection(newDirectionFilter);

        expect(feedOrderComponent.orderByChanged.emit).toHaveBeenCalled();

        feedOrderComponent.setField(newFiled);

        expect(feedOrderComponent.orderByChanged.emit).toHaveBeenCalled();
    });
});
