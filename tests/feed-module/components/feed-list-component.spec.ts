import { async, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { FeedListComponent } from '../../../src/app/feed-module/components/feed-list.component';
import { FeedModule } from '../../../src/app/feed-module';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

let feedListComponent: FeedListComponent,
    modal: NgbModal;

class NgbModalMock {
    open(content: any, options?: NgbModalOptions): any {
        return {
            componentInstance: {
                title: ''
            },
            result: Promise.resolve({})
        };
    }
}

describe('FeedListComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FeedModule
            ],
            providers: [
                { provide: NgbModal, useClass: NgbModalMock }
            ]
        });
    });

    beforeEach(inject([NgbModal],(_modal_: NgbModal) => {
                modal = _modal_;
            }));

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<FeedListComponent> = TestBed.createComponent(FeedListComponent);

            feedListComponent = fixture.componentInstance;
        });
    }));

    it('should show modal when on navigate', () => {
        spyOn(modal, 'open').and.callThrough();

        feedListComponent.navigate({
            link: 'http://google.com',
            title: 'Test',
            categories: [],
            contentSnippet: 'Test',
            publishedDate: 'date'
        });
        expect(modal.open).toHaveBeenCalled();
    });
});
