import { async, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { FeedHistoryComponent } from '../../../src/app/feed-module/components/feed-history.component';
import { FeedModule } from '../../../src/app/feed-module';

let feedHistoryComponent: FeedHistoryComponent;

describe('FeedHistoryComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FeedModule
            ]
        });
    });

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<FeedHistoryComponent> = TestBed.createComponent(FeedHistoryComponent);

            feedHistoryComponent = fixture.componentInstance;
        });
    }));

    it('should emit event if history item is selected', () => {
        let testUrl: string = 'http://google.com';

        spyOn(feedHistoryComponent.onFeedHistoryNavigate, 'emit');

        feedHistoryComponent.navigateToHistoryItem(testUrl);

        expect(feedHistoryComponent.onFeedHistoryNavigate.emit).toHaveBeenCalledWith(testUrl);
    });
});
