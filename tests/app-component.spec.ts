import { async, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { AppModule } from '../src/app/app.module';
import { AppComponent } from '../src/app/app.component';
import { FeedService } from '../src/app/feed-module/services';

let appComponent: AppComponent,
    feedService: FeedService;

describe('AppComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ]
        });
    });

    beforeEach(inject([FeedService], (_feedService_: FeedService) => {
        feedService = _feedService_;
    }));

    beforeEach(async(() => {
        TestBed.compileComponents().then(() => {
            const fixture: ComponentFixture<AppComponent> = TestBed.createComponent(AppComponent);

            appComponent = fixture.componentInstance;
        });
    }));

    it('should load feed on init if has a feed url', () => {
        spyOn(appComponent, 'loadFeed');

        appComponent.feedUrl = 'http://google.lt';

        appComponent.ngOnInit();

        expect(appComponent.loadFeed).toHaveBeenCalled();
    });

    it('should load feed if feed url is changed', () => {
        spyOn(appComponent, 'loadFeed');

        appComponent.changeFeed('http://google.lt');

        expect(appComponent.loadFeed).toHaveBeenCalled();
    });

    it('should not load feed on init if no feed url is specified', () => {
        spyOn(appComponent, 'loadFeed');

        appComponent.ngOnInit();

        expect(appComponent.loadFeed).not.toHaveBeenCalled();
    });

    it('should change sort expression when a new one received', () => {
        let newExpression: string = '-testField';
        appComponent.orderByExpression = [];

        appComponent.orderChanged(newExpression);

        expect(appComponent.orderByExpression).toEqual([newExpression]);
    });

    it('should load feed when a valid form was submitted', () => {
        let newForm: any = {
            invalid: false
        };

        spyOn(appComponent, 'loadFeed');

        appComponent.onSubmit(newForm);

        expect(appComponent.loadFeed).toHaveBeenCalled();
    });

    it('should not load feed when an invalid form was submitted', () => {
        let newForm: any = {
            invalid: true
        };

        spyOn(appComponent, 'loadFeed');

        appComponent.onSubmit(newForm);

        expect(appComponent.loadFeed).not.toHaveBeenCalled();
    });

    it('should call feed service with the specified feed url', () => {
        let mockUrl: string = 'http://google.lt';

        spyOn(feedService, 'getFeed').and.callFake(() => Promise.resolve({}));

        appComponent.feedUrl = mockUrl;

        appComponent.loadFeed();

        expect(feedService.getFeed).toHaveBeenCalledWith(mockUrl);
    });
});
